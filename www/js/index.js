var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);

    },
    onDeviceReady: function() {

     var height= ($( document ).height()-$('.logo').height()-2);
     $("#main").css("height",height);
     document.addEventListener("backbutton", function(e){
        if($.mobile.activePage.is('#pageone')){
            e.preventDefault();
            navigator.app.exitApp();
        }
        else {
            navigator.app.backHistory()
        }
    }, false);
     
     $( window ).hashchange(function() {
        var hash = location.hash;
        if(!(sessionStorage.isLoggedIn === 'true') ) {
          $(location).attr("href","#pageone");
          window.location.reload();   
      }
  });
 }
};

app.initialize();
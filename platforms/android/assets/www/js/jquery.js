$(document).ready(function(){
	sessionStorage.isLoggedIn = sessionStorage.isLoggedIn ? sessionStorage.isLoggedIn : "false";

	$("#login").click(function(){
		$.ajax({
			type:"POST",
			url:"json/data.json",
			success:function(data){
				uname=$("#username").val();
				password=$("#password").val();
				var flag=1;
				data = JSON.parse(data);
				$.each(data,function(index){
					if(data[index].Username==uname && data[index].Password==password){
						flag=2;
						usrname=data[index].Username;
						accts=data[index].Accounts;
						return false;
					}else{
						flag=1;
					}
				});
				if(flag==2){	
					$(location).attr("href","#pagetwo");
					pagetwo(usrname,accts);
					sessionStorage.isLoggedIn = true;
					return false;
				}else{
					$("#alert").removeAttr("style")
				}				
			},
			error:function(httprequest,textstatus,errorthrown){
				alert("Data is not loaded due to " + errorthrown)
			}
		})			
	});

	$("#next").click(function(){
		$("#usrname2").html(usrname);
		$.ajax({
			type:"POST",
			url:"json/merchant.json",
			success:function(data){
				data = JSON.parse(data);
				code=$("#mrchcode").val();
				var flag=1;
				$.each(data,function(index){
					if(data[index].Code==code)
					{
						flag=2;
						merchant=data[index].Username;
						state=data[index].State;
						return false;
					}
					else
					{
						flag=1;
					}
				});
				if(flag==2)
				{	
					$(location).attr("href","#confirm");
					confirm1(merchant,state);
					return false;
				}
				else
				{
					$("#mrch").removeAttr("style")
				}				
			},
			error:function(httprequest,textstatus,errorthrown){
				alert("Data is not loaded due to " + errorthrown)
			}
		})
	});
	$("#next1").click(function(){
		$("#usrname3").html(usrname);
		var OTP = Math.floor(1000 + Math.random() * 9000);
		var d=new Date();
		var date=d.getDate();
		var month= new Array();
		month[0]="Jan";
		month[1]="Feb";
		month[2]="Mar";
		month[3]="Apr";
		month[4]="May";
		month[5]="Jun";
		month[6]="Jul";
		month[7]="Aug";
		month[8]="Sep";
		month[9]="Oct";
		month[10]="Nov";
		month[11]="Dec";
		var h=d.getHours();
		var m=d.getMinutes();
		var s=d.getSeconds();
		var fulldate=date+" "+month[d.getMonth()]+" "+h+":"+m+":"+s;
		$("#date1").html(fulldate);
		$("#oppwd").html(OTP);
		$("#close").click(function(){			
			$(location).attr("href","#otp1");
			return false;
		})
		otppin(OTP);
	});
	function otppin(OTP){
		$("#next2").click(function(){
			$("#usrname4").html(usrname);
			$("#usrname5").html(usrname);
			var enotp=parseInt($("#otpin").val());
			var transactID = Math.floor(1000000000 + Math.random() * 9000000000);
			if(parseInt(OTP)==enotp)
			{
				$("#tranid").html("TXN"+transactID);
				$(location).attr("href","#transucc");
			}
			else
			{
				$(location).attr("href","#transfail");
			}
		});
	}
	$(".logout").click(function(){
		$(location).attr("href","#pageone");
		window.location.reload();		
		sessionStorage.isLoggedIn = 'false';
	})
})
function pagetwo(usrname,accts)
{
	$(document).ready(function(){
		$("#usrname").html(usrname);
		$("#usrname1").html(usrname);
		$.each(accts,function(index){
			$("#acctsmenu ul").append('<li><a href="#" class="ui-btn ui-btn-icon-left ui-icon-star"><span>'+accts[index].Type+'<span style="float: right;">INR '+accts[index].Amount+'</span><div style="font-size:12px;">'+accts[index].CardNo+'</div>');
			$("#account select").append('<option value="'+accts[index].CardNo+'">'+accts[index].Type+', '+accts[index].CardNo+'</option>');
		});
		$("#accountslt").change(function(){
			var selval=	this.value;
			var flag=1;
			$.each(accts,function(index){
				if(selval==accts[index].CardNo)
				{
					flag=2;
					amt=accts[index].Amount;
					return false;
				}
				else
				{
					flag=1;
				}
			});
			if(flag==2)
			{	
				$("#balamt").val(amt);
				
			}
			else
			{
				$("#balamt").val(0);
			}
			seltext=$("#accountslt option:selected").text();
			$("#contype").html(seltext);
			$("#tranacct").html(seltext);
			$("#tranacct1").html(seltext);		
		});
		$("#transamt").change(function(){
			var balamt=parseInt($("#balamt").val());
			var tranamt=parseInt($("#transamt").val());
			if(tranamt > balamt)
		{
			$("#amtless").removeAttr("style");
			$("#next").addClass("ui-state-disabled");
			$("#mrchcode").addClass("ui-state-disabled");
			$("#scan").addClass("ui-state-disabled");
		}
		else
		{
			$("#amtless").css("display","none");
			$("#next").removeClass("ui-state-disabled");
			$("#mrchcode").removeClass("ui-state-disabled");
			$("#scan").removeClass("ui-state-disabled");
		}
			$("#conamt").html("INR "+tranamt);
			$("#trancamt").html("INR "+tranamt);
			$("#trancamt1").html("INR "+tranamt);
		});
	});
$('#scan').click(function(event){
	cordova.plugins.barcodeScanner.scan(
		function (result) {
			$('#mrchcode').val(result.text);
		}, 
		function (error) {
			alert("Scanning failed: " + error);
		}
		);
});
}
function confirm1(merchant,state)
{
	$("#merchname").html(merchant);
	$("#merchstate").html(state);
	$("#payee").html(merchant);
	$("#payeestate").html(state);
	$("#payee1").html(merchant);
	$("#payeestate1").html(state);
}